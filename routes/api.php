<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/ 

//User
$router->post('login', 'AuthController@login');
$router->post('sendInvite', 'UserController@sendInvite');
$router->post('resetPassword', 'UserController@resetPassword');
$router->post('getCountryCode', 'UserController@getCountryCode');
$router->post('addUser', 'UserController@addUser');
$router->get('getUser/{userType}', 'UserController@getUser');
$router->get('getOneUser/{id}', 'UserController@getOneUser');
$router->post('updateUser/{id}', 'UserController@updateUser');
$router->delete('deleteUser/{id}', 'UserController@deleteUser');
$router->post('addAddress', 'UserController@addAddress');
$router->delete('deleteAddress/{id}', 'UserController@deleteAddress');
$router->post('addAccessDetails','UserController@addAccessDetails');
$router->post('updateAccessDetails/{id}','UserController@updateAccessDetails');
$router->delete('deleteAccessDetails/{id}','UserController@deleteAccessDetails');
$router->get('getUserAccessDetail/{id}','UserController@getUserAccessDetail');


//Products
$router->post('addCategory', 'ProductController@addCategory');
$router->post('addProduct', 'ProductController@addProduct');
$router->get('getAllProducts', 'ProductController@getAllProducts');
$router->get('getOneProduct/{id}', 'ProductController@getOneProduct');
$router->put('updateProduct/{id}', 'ProductController@UpdateProduct');
//Tickets
$router->post('createTicket', 'TicketController@createTicket');

//ClientProduct
$router->post('addClientProduct', 'ClientProductController@addClientProduct');
$router->delete('clientProductDelete/{id}', 'ClientProductController@deleteClientProduct');
$router->get('getClientProduct/{id}', 'ClientProductController@getClientProduct');
