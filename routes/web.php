<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/ 

// $router->post('/', function () use ($router) {
//     // return $router->app->version();
//     echo "Welcome";
// });
// $router->get('user/{id}', function ($id) {
//     return 'User '.$id;
// });
// $router->get('profiles', ['as' => 'profile', function () {
//     echo "Welcome";
// }]);
// $router->post('login', 'AuthController@login');