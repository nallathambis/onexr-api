<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FileManagement extends Model
{

    protected $table = 'file_management';


    protected $fillable = [
        'productId',
        'fileType',
        'fileName'
         ];
   public $timestamps = false;

}

