<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class AccessDetails  extends Model
{
    protected $table = 'access_details';

    protected $fillable = [
            'userId',
            'productAdd',
            'productEdit	',
            'productDelete',
            'technicianAdd',
            'technicianEdit	',
            'technicianDelete',
            'clientAdd',
            'clientEdit',
            'clientDelete',
            'ticketAssign',
            
    ];
    
   public $timestamps = false;
}