<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tickets extends Model
{
    protected $table = 'tickets';

    protected $fillable = [
        'clientId',
        'productId',
        'addressId',
    ];
   public $timestamps = false;
}
