<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClientProduct extends Model
{
    protected $table = 'client_products';

    protected $fillable = [
        'clientId',
        'productId',
        'createdAt'
         ];
   public $timestamps = false;

}
