<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductFaq extends Model
{
    protected $table = 'product_faq';

    protected $fillable = [
        'productId',
        'question',
        'answer'
    ];
   public $timestamps = false;
}
