<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductQuery extends Model
{
    protected $table = 'product_query';

    protected $fillable = [
        'productId',
        'query'
    ];
   public $timestamps = false;
}
