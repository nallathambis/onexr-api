<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserAddress extends Model
{
    protected $table = 'address';

    protected $fillable = [
        'clientId',
        'Name',
        'DoorNo',
        'Street',
        'City',
        'State',
        'Country',
        'PIN',
    ];
   public $timestamps = false;
}
