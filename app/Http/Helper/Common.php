<?php
namespace App\Http\Helper;

use Closure;
use App\Models\Category;
use App\Models\FileManagement;
use App\Models\ProductQuery;
use App\Models\ProductFaq;

class Common
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function getCatgDetails($catgId){
        $catgData=(Object)[];
        $getData = Category::where(['id' => $catgId])->get()[0];
        if($getData){
            $catgData = $getData;
        }
        return $catgData;
    }   
    public function getQuery($productId){
        $getData = ProductQuery::where(['productId' => $productId])->get();
        $qData=[];
        if($getData){
            $qData=$getData;
        }
        return $qData;
    }
    public function getFAQ($productId){
        $faqData =ProductFaq::where(['productId' => $productId])->get();
        $fData=[];
        if($faqData){
            $fData = $faqData;
        }
        return $fData;
    }
    public function getFiles($productId,$type){
        $fileData=(Object)[];
        $FileMangement=new FileManagement;
        $getData = FileManagement::where(['productId' => $productId,'fileType' => $type])->get();
        $fData=[];
        if($getData){
            $fData = $getData;
        }
        return $fData;
    }
}   
