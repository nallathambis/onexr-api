<?php

namespace App\Http\Helper;

use Illuminate\Http\Request;

class Validation
{
    public static function validator($inputData, $requiredFields)
    {
        $error = false;
        foreach ($requiredFields as $field) {
            if (empty($inputData[$field])) {
                $error = $field . ' is required!';
            }
        }
        if ($error) {
            echo json_encode(array('status' => false, 'message' => $error, 'data' => ''));
            exit();
        } else {
            return true;
        }
    }
}
