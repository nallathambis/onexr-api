<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserLanguages;
use App\Models\SocialMedia;
use App\Models\UserAddress;
use App\Models\AccessDetails;
use App\Models\Country;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Helper\Response;
use App\Http\Helper\Validation;

class UserController extends Controller
{
    public function sendInvite(Request $request){
        $inputData=$request->input();
        $reqFields = array('email');
        $validation = Validation::validator($inputData, $reqFields);
        
        $email = $inputData['email'];
        
        $sendMail = new \SendGrid\Mail\Mail();
        $sendMail->setFrom(getenv('MAIL_FROM_ADDRESS'), "Admin");
        $sendMail->setSubject(getenv('MAIL_FROM_ADDRESS'));
        $sendMail->addTo($email, "Name");
        $sendMail->addContent("text/plain", "and easy to do anywhere, even with PHP");
        $sendMail->addContent(
            "text/html", "Welcome to OneXR, <a href='https://onexr.com?email='".$email."'invitesent'>Click here </a> to Register"
        );
        $sendgrid = new \SendGrid(getenv('SENDGRID_API_KEY'));
        try {
            $response = $sendgrid->send($sendMail);
            return Response::result(true,"Invite link sent",(object)[]);
        } catch (Exception $e) {
            return Response::result(false,$e->getMessage(),(object)[]);
        }
    }
    public function resetPassword(Request $request){
        $inputData=$request->input();
        $reqFields = array('email');
        $validation = Validation::validator($inputData, $reqFields);
        
        $email = $inputData['email'];
        $getData = User::where(['email' => $email])->paginate(1)[0];
        if(!$getData){
            return Response::result(false,"User not exist",(object)[]);
        }
        $name=$getData['firstName'];
        $sendMail = new \SendGrid\Mail\Mail();
        $sendMail->setFrom(getenv('MAIL_FROM_ADDRESS'), "Admin");
        $sendMail->setSubject(getenv('MAIL_FROM_ADDRESS'));
        $sendMail->addTo($email, $name);
        $sendMail->addContent("text/plain", "and easy to do anywhere, even with PHP");
        $sendMail->addContent(
            "text/html", "To reset your password, <a href='https://onexr.com?email='".$email."'/resetpssword'> Click here</a>"
        );
        $sendgrid = new \SendGrid(getenv('SENDGRID_API_KEY'));
        try {
            $response = $sendgrid->send($sendMail);
            return Response::result(true,"Reset password link sent",(object)[]);
        } catch (Exception $e) {
            return response()->json(["status"=>false, "message"=> $e->getMessage() ,"data"=> (object)[]]);
        }
    }
    public function getCountryCode(Request $request)
    {
        $getData = Country::all();
        if($getData){
            return Response::result(true,"Countries listed sucess",$getData);
        }else{
            return Response::result(false,"Countries not found",(object)[]);
        }
    }
    public function addUser(Request $request){
        $inputData=$request->input();
        $reqFields = array('email','firstName','phoneCode','mobileNumber','userType');
        $validation = Validation::validator($inputData, $reqFields);
        $userData = new User;
        $email = $inputData['email'];
        $firstName = $inputData['firstName'];
        $phoneCode = $inputData['phoneCode'];
        $mobileNumber = $inputData['mobileNumber'];
        $userType = $inputData['userType'];
        $socialAccountUrl = $request->input('socialAccountUrl');
        
        $getData = User::where(['email' => $email])->get();
        if(count($getData)>0){
            return Response::result(false,"Email already exist",(object)[]);
        }
        try {
            $userData->email=$request->input('email');
            $userData->password=$request->input('password')??"";
            $userData->firstName=$request->input('firstName');
            $userData->phoneCode=$request->input('phoneCode');
            $userData->mobileNumber=$request->input('mobileNumber');
            $userData->profileImage=$request->input('profileImage')??"";
            $userData->userType=$request->input('userType');
            $userData->status='offline';
            $userData->createdAt=date('Y-m-d H:i:s');
            $userData->updatedAt=date('Y-m-d H:i:s');
            if($request->input('lastName')){
                $userData->lastName = $inputData['lastName'];
            }
            if($request->hasFile('profileImage')){
                $file = $request->file('profileImage');
                $allowedFileExtension = ['pdf','png','jpg','jpeg'];
                $extension=$file->getClientOriginalExtension();
                
                $check = in_array($extension,$allowedFileExtension);
    
                if($check){
                    $name=time() .$file->getClientOriginalName();
                    // $s3 = new Aws\S3\S3Client([
                    //     'region'  => 'ap-south-1',
                    //     'version' => 'latest',
                    //     'credentials' => [
                    //         'key'    => AWS_ACCESS_KEY,
                    //         'secret' => AWS_SECRET_KEY,
                    //     ]
                    // ]);
                    // $s3Results =  $s3->putObject([
                    //     'Bucket' => 'fixarv2',
                    //     'Key'    => 'OneXR/Callhistory/'.$complaintId.'/'.$videoFileName,
                    //     'Body'   => fopen($source, 'r'),
                    //     'ACL'    => 'public-read',
                    // ]);
                    // $videoPath = $s3Results['ObjectURL'];
                    $file->move('images',$name);
                    $userData->profileImage=$name;
                }
            }
            if($request->input('password')){
                $userData->password=password_hash($userData->password, PASSWORD_DEFAULT);
            }
            $result= $userData->save();
            $insertedUserId=$userData->id;
            if($result==1)
            {
                $userLanguages=new UserLanguages();
                $userLanguages->userId=$insertedUserId;
                $userLanguages->languageId=26;//English id
                $userLanguages->save();

                if($socialAccountUrl){
                    $socialAccountUrl=explode(",", $socialAccountUrl);
                    foreach ($socialAccountUrl as $url){
                        $socialMedia = new SocialMedia;
                        if (strpos($url, 'facebook')) {
                            $socialMedia->userId=$insertedUserId;
                            $socialMedia->accountUrl=$url;
                            $socialMedia->socialMediaType='Facebook';
                            $socialMedia->save();
                        }
                        if (strpos($url, 'youtube')) {
                            $socialMedia->userId=$insertedUserId;
                            $socialMedia->accountUrl=$url;
                            $socialMedia->socialMediaType='Youtube';
                            $socialMedia->save();
                        }
                        if (strpos($url, 'twitter')) {
                            $socialMedia->userId=$insertedUserId;
                            $socialMedia->accountUrl=$url;
                            $socialMedia->socialMediaType='Twitter';
                            $socialMedia->save();
                        }
                        if (strpos($url, 'instagram')) {
                            $socialMedia->userId=$insertedUserId;
                            $socialMedia->accountUrl=$url;
                            $socialMedia->socialMediaType='Instagram';
                            $socialMedia->save();
                        }
                    }
                }
                
                $user = User::where(['id' => $insertedUserId])->get()[0];
                $getLanguageData = UserLanguages::where(['userId' => $user->id])->get();
                $getSocialMediaData = SocialMedia::where(['userId' => $user->id])->get();
                $resultData[]=[
                    'id'=>$user->id,
                    'firstName'=>$user->firstName,
                    'lastName'=>$user->lastName,
                    'email'=>$user->email,
                    'phoneCode'=>$user->phoneCode,
                    'mobileNumber'=>$user->mobileNumber,
                    'profileImage'=>$user->profileImage,
                    'userType'=>$user->userType,
                    'deviceToken'=>$user->deviceToken,
                    'experience'=>$user->experience,
                    'status'=>$user->status,
                    'createdAt'=>$user->createdAt,
                    'updatedAt'=>$user->updatedAt,
                    'activeStatus'=>$user->activeStatus,
                    'userLanguages'=>$getLanguageData,
                    'socialMedia'=>$getSocialMediaData
                ];
                $status=true;
                $info="Data added successfully.";
                $data=$resultData;
            }
            else
            {
                $status=false;
                $info="Data not added";
                $data=(object)[];
            }
            return Response::result($status,$info,$data);
        } catch (Exception $e) {
            return Response::result(false,$e->getMessage(),(object)[]);
        }
    }
    public function getUser($userType){
        try {
            $userData = new User;
            $userLanguages=new UserLanguages();
            $socialMedia = new SocialMedia;
            if($userType=='All'){
                $getData = User::whereNotIn('userType', ['Admin'])->get();
            }else{
                $getData = User::where(['userType' => $userType])->get();
            }
            if(!count($getData)>0){
                return Response::result(false,"Data not found",(object)[]);
            }else{
                foreach($getData as $user){
                    $getLanguageData = UserLanguages::where(['userId' => $user->id])->get();
                    $getSocialMediaData = SocialMedia::where(['userId' => $user->id])->get();
                    $resultData[]=[
                        'id'=>$user->id,
                        'firstName'=>$user->firstName,
                        'lastName'=>$user->lastName,
                        'email'=>$user->email,
                        'phoneCode'=>$user->phoneCode,
                        'mobileNumber'=>$user->mobileNumber,
                        'profileImage'=>$user->profileImage,
                        'userType'=>$user->userType,
                        'deviceToken'=>$user->deviceToken,
                        'experience'=>$user->experience,
                        'status'=>$user->status,
                        'createdAt'=>$user->createdAt,
                        'updatedAt'=>$user->updatedAt,
                        'activeStatus'=>$user->activeStatus,
                        'userLanguages'=>$getLanguageData,
                        'socialMedia'=>$getSocialMediaData
                    ];

                }
                return Response::result(true,'Data get success',$resultData);
            } 
        } catch (Exception $e) {
            return Response::result(false,$e->getMessage(),(object)[]);
        }
    }
    public function getOneUser($id){
        try {
            $getData = User::where(['id' => $id])->get()[0];
            if(!$getData){
                return Response::result(false,"Data not found",(object)[]);
            }else{
                $getLanguageData = UserLanguages::where(['userId' => $id])->get();
                $getSocialMediaData = SocialMedia::where(['userId' => $id])->get();
                $resultData[]=[
                    'id'=>$getData->id,
                    'firstName'=>$getData->firstName,
                    'lastName'=>$getData->lastName,
                    'email'=>$getData->email,
                    'phoneCode'=>$getData->phoneCode,
                    'mobileNumber'=>$getData->mobileNumber,
                    'profileImage'=>$getData->profileImage,
                    'getDataType'=>$getData->getDataType,
                    'deviceToken'=>$getData->deviceToken,
                    'experience'=>$getData->experience,
                    'status'=>$getData->status,
                    'createdAt'=>$getData->createdAt,
                    'updatedAt'=>$getData->updatedAt,
                    'activeStatus'=>$getData->activeStatus,
                    'userLanguages'=>$getLanguageData,
                    'socialMedia'=>$getSocialMediaData
                ];

                return Response::result(true,'Data get success',$resultData);
            } 
        } catch (Exception $e) {
            return Response::result(false,$e->getMessage(),(object)[]);
        }
    }
    
    public function updateUser(Request $request,$id){
        $inputData=$request->input();        
        $userData = new User;
        $getData = User::where(['id' => $id])->get();
        if(!count($getData)>0){
            return Response::result(false,"User not exist",(object)[]);
        }
        $updateArr=[];
        try {
            if($request->input('firstName')){
                $firstName = $inputData['firstName'];
                $userData->firstName = $firstName;
                $updateArr['firstName'] = $firstName;
            }
            if($request->input('phoneCode')){
                $phoneCode = $inputData['phoneCode'];
                $userData->phoneCode = $phoneCode;
                $updateArr['phoneCode'] = $phoneCode;
            }
            if($request->input('mobileNumber')){
                $mobileNumber = $inputData['mobileNumber'];
                $userData->mobileNumber = $mobileNumber;
                $updateArr['mobileNumber'] = $mobileNumber;
            }
            if($request->input('userType')){
                $userType = $inputData['userType'];
                $userData->userType = $inputData['userType'];
                $updateArr['userType'] = $userType;
            }
            if($socialAccountUrl=$request->input('socialAccountUrl')){
                $getSocialMediaData = SocialMedia::where(['userId' => $id])->get();
                if(count($getSocialMediaData)>0){                    
                    $whereArray = array('userId' => $id);
                    $deleteSocialMedia=SocialMedia::where($whereArray)->delete();
                }
                $socialAccountUrl=explode(",", $socialAccountUrl);
                foreach ($socialAccountUrl as $url){
                    $socialMedia = new SocialMedia;
                    if (strpos($url, 'facebook')) {
                        $socialMedia->userId=$id;
                        $socialMedia->accountUrl=$url;
                        $socialMedia->socialMediaType='Facebook';
                        $socialMedia->save();
                    }
                    if (strpos($url, 'youtube')) {
                        $socialMedia->userId=$id;
                        $socialMedia->accountUrl=$url;
                        $socialMedia->socialMediaType='Youtube';
                        $socialMedia->save();
                    }
                    if (strpos($url, 'twitter')) {
                        $socialMedia->userId=$id;
                        $socialMedia->accountUrl=$url;
                        $socialMedia->socialMediaType='Twitter';
                        $socialMedia->save();
                    }
                    if (strpos($url, 'instagram')) {
                        $socialMedia->userId=$id;
                        $socialMedia->accountUrl=$url;
                        $socialMedia->socialMediaType='Instagram';
                        $socialMedia->save();
                    }
                }
            }
            if($userLanguageIds=$request->input('languages')){
                $getUserLanguages = UserLanguages::where(['userId' => $id])->get();
                if(count($getUserLanguages)>0){
                    $whereArray = array('userId' => $id);
                    UserLanguages::where($whereArray)->delete();
                }
                $userLanguagesSplit=explode(",", $userLanguageIds);
                foreach ($userLanguagesSplit as $languageId){
                    $userLanguages=new UserLanguages();
                    $userLanguages->userId=$id;
                    $userLanguages->languageId=$languageId;
                    $userLanguages->save();
                }
            }
            if($password=$request->input('password')){
                $userData->password=password_hash($inputData['password'], PASSWORD_DEFAULT);
                $updateArr['password'] = password_hash($inputData['password'], PASSWORD_DEFAULT);
            }
            if($request->hasFile('profileImage')){
                $oldUrl= $getData[0]->profileImage;
                if($oldUrl){
                    unlink('images/'.$oldUrl);
                }
                $file = $request->file('profileImage');
                $allowedFileExtension = ['pdf','png','jpg','jpeg'];
                $extension=$file->getClientOriginalExtension();
                
                $check = in_array($extension,$allowedFileExtension);

                if($check){
                    $name=time() .$file->getClientOriginalName();
                    $file->move('images',$name);
                    $updateArr['profileImage'] = $name;
                }
            }
            if($experience=$request->input('experience')) {
                $updateArr['experience'] = $experience;
            }
            if($deviceToken=$request->input('deviceToken')) {
                $updateArr['deviceToken'] = $deviceToken;
            }
            if($address=$request->input('address')){
                $addressVal=json_decode($address);
                $addressId=$addressVal->id;
                if($addressVal->name!="" && $addressId){
                    $addressData['clientId'] = $id;
                    $addressData['name'] = $addressVal->name;
                    $addressData['doorNo'] = $addressVal->doorNo;
                    $addressData['street'] = $addressVal->street;
                    $addressData['city'] = $addressVal->city;
                    $addressData['state'] = $addressVal->state;
                    $addressData['country'] = $addressVal->country;
                    $addressData['pin'] = $addressVal->pin;

                    $updatedData= UserAddress::where('id', $addressId)  
                        ->limit(1)  
                        ->update($addressData);
                    if(!$updatedData){
                        return Response::result(false,'Cannot update address',(object)[]);
                    }
                }
            }
            if($request->input('status')){
                $updateArr['status'] = $status;
            }
            $updateArr['updatedAt'] = date('Y-m-d H:i:s');

            $updatedData= User::where('id', $id)->limit(1)->update($updateArr);
            if($updatedData==1)
            {
                $user = User::where(['id' => $id])->get()[0];
                $getLanguageData = UserLanguages::where(['userId' => $id])->get();
                $getSocialMediaData = SocialMedia::where(['userId' => $id])->get();
                $resultData[]=[
                    'id'=>$user->id,
                    'firstName'=>$user->firstName,
                    'lastName'=>$user->lastName,
                    'email'=>$user->email,
                    'phoneCode'=>$user->phoneCode,
                    'mobileNumber'=>$user->mobileNumber,
                    'profileImage'=>$user->profileImage,
                    'userType'=>$user->userType,
                    'deviceToken'=>$user->deviceToken,
                    'experience'=>$user->experience,
                    'status'=>$user->status,
                    'createdAt'=>$user->createdAt,
                    'updatedAt'=>$user->updatedAt,
                    'activeStatus'=>$user->activeStatus,
                    'userLanguages'=>$getLanguageData,
                    'socialMedia'=>$getSocialMediaData
                ];
                $status=true;
                $info="Data updated successfully.";
                $data=$resultData;
            }
            else
            {
                $status=false;
                $info="Fail to update";
                $data=(object)[];
            }
            return Response::result($status,$info,$data);
        } catch (Exception $e) {
            return Response::result(false,$e->getMessage(),(object)[]);
        }
    }
    public function deleteUser($id){
        $userData = new User;
        $getData = User::where(['id' => $id])->get();
        if(!count($getData)>0){
            return Response::result(false,"User not exist",(object)[]);
        }
        $userType=$getData[0]->userType;
        $deleteArr=[];
        try {
            $getSocialMediaData = SocialMedia::where(['userId' => $id])->get();
            if(count($getSocialMediaData)>0){                    
                $whereArray = array('userId' => $id);
                $deleteSocialMedia=SocialMedia::where($whereArray)->delete();
            }
            $getUserLanguages = UserLanguages::where(['userId' => $id])->get();
            if(count($getUserLanguages)>0){
                $whereArray = array('userId' => $id);
                UserLanguages::where($whereArray)->delete();
            }
            $oldUrl= $getData[0]->profileImage;
            if($oldUrl){
                unlink('images/'.$oldUrl);
            }
            $whereArray = array('userId' => $id);
            $deletedData= User::where('id', $id)->delete();
            if($deletedData==1)
            {
                $status=true;
                $info=$userType." deleted successfully.";
                $data=(object)[];
            }
            else
            {
                $status=false;
                $info="Fail to delete";
                $data=(object)[];
            }
            return Response::result($status,$info,$data);
        } catch (Exception $e) {
            return Response::result(false,$e->getMessage(),(object)[]);
        }
    }
    public function addAddress(Request $request){
        $inputData=$request->input();
        $reqFields = array('clientId','name','doorNo','street','city','state','country','pin');
        $validation = Validation::validator($inputData, $reqFields);
        $userData = new User;
        
        $getData = User::where(['id' => $request->input('clientId')])->get();
        if(!count($getData)>0){
            return Response::result(false,"User not exist",(object)[]);
        }
        $userAddress=new UserAddress();
        try {
            $userAddress->clientId=$request->input('clientId');
            $userAddress->name=$request->input('name');
            $userAddress->doorNo=$request->input('doorNo');
            $userAddress->street=$request->input('street');
            $userAddress->city=$request->input('city');
            $userAddress->state=$request->input('state');
            $userAddress->country=$request->input('country');
            $userAddress->pin=$request->input('pin');
            $userAddress->updatedAt=date('Y-m-d H:i:s');
            
            $result= $userAddress->save();
            $insertedAddressId=$userAddress->id;
            if($result==1)
            {
                $userAddressResult = UserAddress::where(['id' => $insertedAddressId])->get()[0];
                $resultData=[
                    'clientId'=>$userAddressResult->clientId,
                    'name'=>$userAddressResult->name,
                    'doorNo'=>$userAddressResult->doorNo,
                    'street'=>$userAddressResult->street,
                    'city'=>$userAddressResult->city,
                    'state'=>$userAddressResult->state,
                    'country'=>$userAddressResult->country,
                    'pin'=>$userAddressResult->pin,
                    'updatedAt'=>$userAddressResult->updatedAt,
                ];
                $status=true;
                $info="Address added successfully.";
                $data=$resultData;
            }
            else
            {
                $status=false;
                $info="Address not added";
                $data=(object)[];
            }
            return Response::result($status,$info,$data);
        } catch (Exception $e) {
            return Response::result(false,$e->getMessage(),(object)[]);
        }
    }
    public function deleteAddress($id)
    {
        $userAddress = new UserAddress();
        $getData = UserAddress::where(['id' => $id])->get();
        if(!count($getData)>0){
            return Response::result(false,"Address not exist",(object)[]);
        }
        $deleteArr=[];
        try {
            $whereArray = array('id' => $id);
            $deletedData=UserAddress::where($whereArray)->delete();
            
            if($deletedData==1)
            {
                return Response::result(true,'Address deleted successfully',(object)[]);
            }
            else
            {
                return Response::result(false,'Fail to delete',(object)[]);
            }
        } catch (Exception $e) {
            return Response::result(false,$e->getMessage(),(object)[]);
        }
    }
    public function addAccessDetails(Request $request){
        $accessDetails = new AccessDetails();
        $accessDetails->userId=$request->input('userId');
        $accessDetails->productAdd	=$request->input('productAdd');
        $accessDetails->productEdit=$request->input('productEdit');
        $accessDetails->productDelete=$request->input('productDelete');
        $accessDetails->technicianAdd=$request->input('technicianAdd');
        $accessDetails->technicianEdit=$request->input('technicianEdit');
        $accessDetails->technicianDelete=$request->input('technicianDelete');
        $accessDetails->clientAdd=$request->input('clientAdd');
        $accessDetails->clientEdit=$request->input('clientEdit');
        $accessDetails->clientDelete=$request->input('clientDelete');
        $accessDetails->ticketAssign=$request->input('ticketAssign');
        if($accessDetails->save()){
            
        $lastInsertedID=$accessDetails->id;
        $getAccessDetailsData=AccessDetails::where(['id' =>$lastInsertedID])->get();
        $status=true;
        $info="Data added successfully.";}
        else{
            $status=false;
            $info="something went wrong";
        }
        return Response::result($status,$info,$getAccessDetailsData);
    }
    public function updateAccessDetails(Request $request,$id){
        $inputData=$request->input();
        $users = new User();
        $accessDetails = new AccessDetails();
        $userId=User::where(['id'=>$id,'userType'=>'Member'])->get();
        if(!count($userId)>0){
            return Response::result(false,"User not exist",(object)[]);
        }
        $updateArr=[];
        try {
            if($request->input('productAdd')){
                $productAdd = $inputData['productAdd'];
                $users->productAdd = $productAdd;
                $updateArr['productAdd'] = $productAdd;
            }
            if($request->input('productEdit')){
                $productEdit = $inputData['productEdit'];
                $users->productEdit = $productEdit;
                $updateArr['productEdit'] = $productEdit;
            }
            if($request->input('productDelete')){
                $productDelete = $inputData['productDelete'];
                $users->productDelete = $productDelete;
                $updateArr['productDelete'] = $productDelete;
            }
            if($request->input('technicianAdd')){
                $technicianAdd = $inputData['technicianAdd'];
                $users->technicianAdd = $technicianAdd;
                $updateArr['technicianAdd'] = $technicianAdd;
            }
            if($request->input('technicianEdit')){
                $technicianEdit = $inputData['technicianEdit'];
                $users->technicianEdit = $technicianEdit;
                $updateArr['technicianEdit'] = $technicianEdit;
            }
            if($request->input('technicianDelete')){
                $technicianDelete = $inputData['technicianDelete'];
                $users->technicianDelete = $technicianDelete;
                $updateArr['technicianDelete'] = $technicianDelete;
            }
            if($request->input('clientAdd')){
                $clientAdd = $inputData['clientAdd'];
                $users->clientAdd = $clientAdd;
                $updateArr['clientAdd'] = $clientAdd;
            }
            if($request->input('clientEdit')){
                $clientEdit = $inputData['clientEdit'];
                $users->clientEdit = $clientEdit;
                $updateArr['clientEdit'] = $clientEdit;
            }
            if($request->input('clientDelete')){
                $clientDelete = $inputData['clientDelete'];
                $users->clientDelete = $clientDelete;
                $updateArr['clientDelete'] = $clientDelete;
            }
            if($request->input('ticketAssign')){
                $ticketAssign = $inputData['ticketAssign'];
                $users->ticketAssign = $ticketAssign;
                $updateArr['ticketAssign'] = $ticketAssign;
            }
            $accessDetails->userId=$userId;
            if($updatedData=AccessDetails::where('userId',$id)->update($updateArr)){
            $getAccessDetailsData=AccessDetails::where('id',$id)->get();
            $status=true;
            $info="Data added successfully.";}
        
            else{
            $status=false;
            $info="something went wrong";
            }
            return Response::result($status,$info,$getAccessDetailsData);
        }catch (Exception $e) {
        return Response::result(false,$e->getMessage(),(object)[]);
        }
    }
    public function getUserAccessDetail(Request $request,$id){
        $accessDetails = new AccessDetails();
        $getData=AccessDetails::where(['userId'=>$id])->get();
        if(count($getData)>0){
            $status=true;
            $info='Data listed successfully';
            }
        else{
            $status=false;
            $info='Data not found';
        }
        return Response::result($status,$info,$getData);
    
    }
    public function deleteAccessDetails(Request $request,$id){
        $accessDetails = new AccessDetails();
        if($deleteAccessDetails=AccessDetails::where(['id'=>$id])->delete())
        $msg='Data deleted successfully';
        else{
        $msg='something went wrong';
        }
        return response()->json($msg);
    }
}