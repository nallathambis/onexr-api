<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ClientProduct;
use App\Models\User;
use App\Models\Product;
use App\Http\Helper\Response;

class ClientProductController extends Controller
{
    public function addClientProduct(Request $request)
    {
        $ClientProduct = new ClientProduct;
        $userId =  $ClientProduct->clientId=$request->input('clientId');
        $productId= $ClientProduct->ProductId=$request->input('productId');
        $ClientProduct->createdAt=date('Y-m-d H:i:s');
        $getUserData=User::where(['id'=>$userId,'userType'=>'Client'])->first();
        if($getUserData)
        {
            $getProductData =Product::where(['id'=>$productId])->first();
            if($getProductData)
            {
                $result= $ClientProduct->save();
                if($result==1)
                {
                    $status=true;
                    $info="Data added successfully.";
                }
                else
                {
                    $status=false;
                    $info="Data not added successfully.";
                }
                return Response::result($status,$info);
            }
            else{
                $status=false;
                $info="productid does not  exist.";
                return Response::result($status,$info);
                }
        }
        else{
            $status=false;
            $info="User not found";
            return Response::result($status,$info);
        }
    }

    public function deleteClientProduct(Request $request,$id)
    {
        $ClientProduct = new ClientProduct;
        $res=ClientProduct::where('id',$id)->delete();
        if($res==1)
        {
            $status=true;
            $info="Data deleted successfully.";
        }
        else
        {
            $status=false;
            $info="Data not found.";
        }
        return Response::result($status,$info);
    }

    public function getClientProduct(Request $request,$id)
    {
    $getProductData=Product::get('id','productName');
    //check product
    $ClientProduct = ClientProduct::where(['productid'=>$id])->get();
    $res=[];
    if(count($ClientProduct)>0){
        foreach($ClientProduct as $getClientProduct)
        {
            $Id=$getClientProduct->clientId;
            $UserData=new User();
            $Userdetails = User::where(['id'=>$Id])->get()[0];
            $res[]=[
                'id'=>$Userdetails->id,
                'firstName'=>$Userdetails->firstName,
                'email'=>$Userdetails->email,
                'mobileNumber'=>$Userdetails->mobileNumber,
                'userType'=>$Userdetails->userType,
                'status'=>$Userdetails->status,
                'createdAt'=>$Userdetails->createdAt,
                'updatedAt'=>$Userdetails->updatedAt,
                'activeStatus'=>$Userdetails->activeStatus
            ];
        }
        $status=true;
        $info="Data Listed successfully.";
        $data=$res;
    }else{
        $status=false;
        $info="Data not found.";
        $data=(object)[];
    }
    return Response::result($status,$info,$data);
    }
}