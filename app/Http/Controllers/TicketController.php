<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Tickets;
use App\Http\Helper\Response;
use App\Http\Helper\Validation;

class TicketController extends Controller
{
    public function createTicket(Request $request){
        $inputData=$request->input();
        $reqFields = array('clientId','productId','addressId');
        $validation = Validation::validator($inputData, $reqFields);
        $ticketData = new Tickets;
        $clientId = $inputData['clientId'];
        $productId = $inputData['productId'];
        $addressId = $inputData['addressId'];
        
        try {
            $ticketData->clientId=$request->input('clientId');
            $ticketData->productId=$request->input('productId')??"";
            $ticketData->addressId=$request->input('addressId');

            $ticketData->queryId=$request->input('queryId');
            $ticketData->ticketDescription=$request->input('ticketDescription')??"";
            $ticketData->scheduleCallback=$request->input('scheduleCallback');
            $ticketData->status='Open';
            $ticketData->createdAt=date('Y-m-d H:i:s');
            $ticketData->updatedAt=date('Y-m-d H:i:s');

            if($request->input('technicianId')){
                $ticketData->technicianId=$request->input('technicianId');
            }
            if($request->input('queryId')){
                $ticketData->queryId = $inputData['queryId'];
            }
            if($request->input('ticketDescription')){
                $ticketData->ticketDescription = $inputData['ticketDescription'];
            }
            if($request->input('scheduleCallback')){
                $ticketData->scheduleCallback = $inputData['scheduleCallback'];
            }
          
            $result= $ticketData->save();
            $insertedTicketId=$ticketData->id;
            if($result==1)
            {
                $ticket = Tickets::where(['id' => $insertedTicketId])->get()[0];
                $resultData[]=[
                    'id'=>$ticket->id,
                    'clientId'=>$ticket->clientId,
                    'technicianId'=>$ticket->technicianId,
                    'productId'=>$ticket->productId,
                    'queryId'=>$ticket->queryId,
                    'ticketDescription'=>$ticket->ticketDescription,
                    'addressId'=>$ticket->addressId,
                    'scheduleCallback'=>$ticket->scheduleCallback,
                    'status'=>$ticket->status,
                    'createdAt'=>$ticket->createdAt,
                    'updatedAt'=>$ticket->updatedAt,
                ];
            return Response::result(true,'Ticket created successfully',$resultData);
            }
            else
            {
                return Response::result(false,'Failed to create ticket',(object)[]);
            }
        } catch (Exception $e) {
            return Response::result(false,$e->getMessage(),(object)[]);
        }
    }
}
