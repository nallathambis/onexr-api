<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Helper\Validation;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login']]);
    }

    public function login(Request $request)
    {
        $postData=$request->input();
        $requiredFields = array('email', 'password');
        $validation = Validation::validator($postData, $requiredFields);
        if($validation){
            $credentials = request(['email', 'password']);
            if (!$token = auth()->attempt($credentials)) {
                return response()->json(["status"=>false, "message"=>"Unauthorized","data"=> (object)[]], 401);
            }
            $expires_in=auth()->factory()->getTTL() * 60;
            $email = $postData['email'];
            $password = $postData['password'];
            $getData = User::where(['email' => $email])->paginate(1)[0];
            if($getData){
                if(password_verify($password, $getData['password'])  && ($getData['email'] == $email)){
                    unset($getData['password']);
                    return response()->json(['token' => $token,"status"=>true, "message"=>"Login Success","data"=> $getData]);
                }else{
                    return response()->json(["status"=>false, "message"=>"Invalid password","data"=> (object)[]]);
                }
            }else{
                return response()->json(["status"=>false, "message"=>"Invalid email","data"=> (object)[]]);
            }
        }
    }
}