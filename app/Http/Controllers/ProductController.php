<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductFaq;
use App\Models\ProductQuery;
use App\Models\FileManagement;
use App\Http\Helper\Response;
use App\Http\Helper\Validation;
use App\Http\Helper\Common;

class ProductController extends Controller
{
    public function addCategory(Request $request){
    $inputData=$request->input();
    $reqFields = array('name');
    $validation = Validation::validator($inputData, $reqFields);
    $categoryData = new Category;
    $name = $inputData['name'];
    $status = $inputData['status']??"Active";
    
    try {
        $categoryData->name=$request->input('name');
        $categoryData->status=$request->input('status')??"";

        $result= $categoryData->save();
        $insertedCategoryId=$categoryData->id;
        if($result==1)
        {
            $category = Category::where(['id' => $insertedCategoryId])->get()[0];
            $resultData=[
                'id'=>$category->id,
                'name'=>$category->name,
                'status'=>$category->status,
            ];
        return Response::result(true,'Category created successfully',$resultData);
        }
        else
        {
            return Response::result(false,'Failed to create category',(object)[]);
        }
    } catch (Exception $e) {
        return Response::result(false,$e->getMessage(),(object)[]);
    }
    }
    public function addProduct(Request $request)
    {
    try{
        $inputData=$request->input();
        $reqFields = array('productName','categoryId');
        $validation = Validation::validator($inputData, $reqFields);
        $product = new Product;
        $product->productName=$request->input('productName');
        $product->categoryId=$request->input('categoryId');
        $product->description=$request->input('description');
        $product->status=$request->input('status');
        $product->createdAt=date('Y-m-d H:i:s');
        $product->updatedAt=date('Y-m-d H:i:s');
        $result= $product->save();
        $lastInsertId =$product->id;

        if($faq=$request->input('faq')){
            $productFaqData=json_decode($faq);
            foreach($productFaqData as $faqVal){
                $productFaq = new ProductFaq;
                $productFaq->productId=$lastInsertId;
                $productFaq->question=$faqVal->question;
                $productFaq->answer=$faqVal->answer;
                $faqData= $productFaq->save();
            }
        }
        $productQuery = new ProductQuery;
        if($query=$request->input('query')){
            $productQueryData=json_decode($query);
            foreach($productQueryData as $queryVal){
                $productQuery->productId=$lastInsertId;
                $productQuery->query=$queryVal;
                $queryData= $productQuery->save();
            }
        }
        if($images=$request->file('images')){
            foreach($images as $image)
            {
                $imageName = md5(rand(1000,10000));
                $ext = strtolower($image->getClientOriginalExtension());
                $imageFullName = $imageName.'.'.$ext;
                $uploadPath = 'images/';
                $imagUrl = $uploadPath.$imageFullName;
                $image->move($uploadPath,$imageFullName);

                $FileManagement = new FileManagement();
                $FileManagement->fileName=$imagUrl;
                $FileManagement->productId=$lastInsertId;
                $FileManagement->fileType="Image";
                $imagedata=$FileManagement->save();
            }
        }

        if($documents=$request->file('documents')){
            foreach($documents as $document)
            {
                $documentName = md5(rand(1000,10000));
                $ext = strtolower($document->getClientOriginalExtension());
                $documentFullName = $documentName.'.'.$ext;
                $uploadPath = 'documents/';
                $documentUrl = $uploadPath.$documentFullName;
                $document->move($uploadPath,$documentFullName);

                $FileManagement = new FileManagement();
                $FileManagement->fileName=$documentUrl;
                $FileManagement->productId=$lastInsertId;
                $FileManagement->fileType="Document";
                $documentfile=$FileManagement->save();
            }
        }

        if($videos=$request->file('videos')){
            foreach($videos as $video)
            {
                $videoName = md5(rand(1000,10000));
                $ext = strtolower($video->getClientOriginalExtension());
                $videoFullName = $videoName.'.'.$ext;
                $uploaPath = 'videos/';
                $videoUrl = $uploaPath.$videoFullName;
                $video->move($uploaPath,$videoFullName);

                $FileManagement = new FileManagement();
                $FileManagement->fileName=$videoUrl;
                $FileManagement->productId=$lastInsertId;
                $FileManagement->fileType="Video";
                $videofile=$FileManagement->save();
            }
        }

        if($assets=$request->file('assets')){
            foreach($assets as $asset)
            {
                $assetName = md5(rand(1000,10000));
                $ext = strtolower($asset->getClientOriginalExtension());
                $assetFullName = $assetName.'.'.$ext;
                $uploadPath = 'assets/';
                $assetUrl = $uploadPath.$assetFullName;
                $asset->move($uploadPath,$assetFullName);

                $FileManagement = new FileManagement();
                $FileManagement->fileName=$assetUrl;
                $FileManagement->productId=$lastInsertId;
                $FileManagement->fileType="Asset";
                $assetfile=$FileManagement->save();
            }
        }
        if($result==1)
        {
            $getData = Product::where(['id' => $lastInsertId])->get()[0];
            if(!$getData){
                return Response::result(false,"Data not found",(object)[]);
            }else{
                $common=new Common();
                $resultData=[
                    "id"=>$getData->{'id'},
                    "productName"=>$getData->{'productName'},
                    "description"=>$getData->{'description'},
                    "status"=> $getData->{'status'},
                    "createdAt"=>$getData->{'createdAt'},
                    "updatedAt"=>$getData->{'updatedAt'},
                    "thumbnailImage"=>"https://fixarv2.s3.ap-south-1.amazonaws.com/FixAR/video-placeholder.jpg",
                    "category"=>$common->getCatgDetails($getData->{'categoryId'}),
                    "productImage"=>$common->getFiles($lastInsertId,"Image"),
                    "productVideo"=>$common->getFiles($lastInsertId,"Video"),
                    "productDocument"=>$common->getFiles($lastInsertId,"Audio"),
                    "productDocument"=>$common->getFiles($lastInsertId,"Document"),
                    "query"=>$common->getQuery($getData->{'id'}),
                    "faq"=>$common->getFAQ($getData->{'id'}),
                ];
                return Response::result(true,'Data added successfully',$resultData);
            }
        }else{
            return Response::result(false,'Cannot add Product',(object)[]);
        }
        }
        catch(Exception $ex){
            return Response::result(false,$e->getMessage(),(object)[]);
        }
    }
    public function updateProduct(Request $request, $id)
    {
        $FileManagement = new FileManagement;
        
        $ProductQuery = new ProductQuery;
        $ProductFaqDelete = ProductQuery::where('productId',$id)->delete();
        $ProductQuery->productId = $id;
        $ProductQuery->query=$request->input('query');
        $QueryData= $ProductQuery->save();

        $ProductFaq = new ProductFaq;
        $ProductFaqDelete = ProductFaq::where('productId',$id)->delete();
        $ProductFaq->productId = $id;
        $ProductFaq->question=$request->input('question');
        $ProductFaq->answer=$request->input('answer');
        $FaqData= $ProductFaq->save();

        if($FaqData==1)
        {
            $status=true;
            $info="Data updated successfully.";
        }
        else
        {
            $status=false;
            $info="Data not updated successfully.";
        }
        return Response::result($status,$info);
    }
    public function getAllProducts()
    {
        $data= Product::all();
        $status=true;
        $info="Data listed successfully.";
        return Response::result($status,$info,$data);
    }

    public function getOneProduct($id)
    {
        $data= Product::find($id);
        $status=true;
        $info="Data listed successfully.";
        return Response::result($status,$info,$data);
    }
}
